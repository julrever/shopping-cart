import { Component, OnInit, ViewChild } from '@angular/core';
import { Item } from '../services/item';
import { Category } from '../services/category';
import { DataService } from '../services/data.service';
import { DataTableComponent } from '../data-table/data-table.component';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  categories: Array<Category>;
  items: Array<Item> = [];
  isSelected = false;
  isLoading = false;
  category = null;

  @ViewChild(DataTableComponent, {static: false}) dataTable: DataTableComponent;

  constructor(private dataService: DataService) { 
    dataService.catalogItems$.subscribe(
      catalogItems => {
        this.items = [];
        Object.assign(this.items, catalogItems);
      }
    );
    dataService.categories$.subscribe(
      categories => this.categories = categories
    );
  }

  ngOnInit() {
  }

  filter() {
    return this.items.filter(item => item.parent !== null && item.parent.name === this.category);
  }

  addToCart() {
    this.dataService.addToCart(this.dataTable.selection.selected);
    this.dataTable.selection.clear();
    this.isSelected = false;
  }

}
