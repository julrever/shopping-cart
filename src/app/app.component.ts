import { Component, EventEmitter } from '@angular/core';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  cartQuantity: number;

  constructor(private DataService: DataService) {
    DataService.cartItems$.subscribe(
      cartItems => this.cartQuantity = cartItems.length
    )
   }

  ngOnInit(){
    this.DataService.getData();
  }


}
