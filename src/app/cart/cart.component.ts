import { Component, OnInit, ViewChild } from '@angular/core';
import { Item } from '../services/item';
import { DataService } from '../services/data.service';
import { DataTableComponent } from '../data-table/data-table.component';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  chosenItems: Array<Item>;
  items: Array<Item>;
  isSelected = false;

  @ViewChild(DataTableComponent, {static: false}) dataTable: DataTableComponent;

  constructor(private dataService: DataService) { 
    dataService.cartItems$.subscribe(
      cartItems => {
        this.items = [];
        Object.assign(this.items, cartItems);
      }
    );
  }

  ngOnInit() {
  }

  removeFromCart() {
    this.dataService.removeFromCart(this.dataTable.selection.selected);
    this.dataTable.selection.clear();
    this.isSelected = false;
  }

}
