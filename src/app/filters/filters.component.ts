import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Category } from '../services/category';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
  selectedValue: string;
  @Input() categories: Array<Category>;
  @Output() selectedCategory: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  unselect() {
    this.selectedValue = undefined;
    this.selectedCategory.emit(null);
  }

}
