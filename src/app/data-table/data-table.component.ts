import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ViewChild } from '@angular/core'
import { SelectionModel } from '@angular/cdk/collections';
import { Item } from '../services/item';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  dataSource = new MatTableDataSource<Item>();
  @Input() set items(value: Array<Item>){
    this.dataSource.data = value;
    this.dataSource.sort = this.sort;
  };
  @Output() isSelected: EventEmitter<boolean> = new EventEmitter();
  displayedColumns: string[] = ['select', 'name', 'group', 'price'];
  selection = new SelectionModel(true, []);


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  checkboxClick (row) {
    this.selection.toggle(row)
    if (this.selection.selected.length > 0) {
      this.isSelected.emit(true);
    } else {
      this.isSelected.emit(false);
    }
  }

  masterToggle() {
    if (this.isAllSelected()) {
      this.selection.clear();
      this.isSelected.emit(false);
    } else {
      this.dataSource.data.forEach(row => this.selection.select(row));
      this.isSelected.emit(true);
    }
  }

  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  ngOnInit() {
  }

}