export class Item {
    id: number;
    lastChange: string;
    name: string;
    price: number;
    parent: {
        id: number;
        name: string;
    }
}