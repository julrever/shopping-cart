import { Injectable } from '@angular/core';
import { Item } from './item';
import { Category } from './category';
import { GetItemsService } from './get-items.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private catalogItems = new BehaviorSubject<Item[]>([]);
  private categories = new BehaviorSubject<Category[]>([]);
  private cartItems = new BehaviorSubject<Item[]>([]);

  catalogItems$ = this.catalogItems.asObservable();
  categories$ = this.categories.asObservable();
  cartItems$ = this.cartItems.asObservable();

  constructor(private GetItems: GetItemsService) { }

  getData() {
    this.GetItems.getAllItems().subscribe(
      data => this.catalogItems.next(data),
      err => console.log(err)
    );

    this.GetItems.getAllCategories().subscribe(
      data => { 
        for (let category of data) {
          if (!category['name']) {
            data = data.filter(obj => obj !== category);
          }
        }
        this.categories.next(data)
      }, 
      err => console.log(err)
    );
  };

  addToCart(items: Array<Item>) {
    this.moveItemsFromTo(items, this.catalogItems, this.cartItems);
  };

  removeFromCart(items: Array<Item>) {
    this.moveItemsFromTo(items, this.cartItems, this.catalogItems);
  }

  moveItemsFromTo(items: Array<Item>, 
                  from: BehaviorSubject<Item[]>, 
                  to: BehaviorSubject<Item[]>) {
    var fromArray = from.getValue();
    var toArray = to.getValue();
    for (let item of items) {
      var currentIndex = fromArray.map(obj => obj.id).indexOf(item.id);
      fromArray.splice(currentIndex, 1);
      toArray.push(item);
    }
    from.next(fromArray);
    to.next(toArray);
  };

}
