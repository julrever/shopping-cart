import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { Item } from "../services/item";
import { Category } from "../services/category";

@Injectable({
  providedIn: 'root'
})
export class GetItemsService {

  constructor(private httpClient: HttpClient) { }

  getItemsUrl = 'https://ssdev.superagent.ru/TestApp/Values/GetAll';
  getCategoriesUrl = 'https://ssdev.superagent.ru/TestApp/Values/GetParents';
  getAllItems(): Observable<Item[]> {
    return this.httpClient.get<Item[]>(this.getItemsUrl).pipe(
      catchError(this.handleError)
    );
  }

  getAllCategories(): Observable<Category[]> {
    return this.httpClient.get<Category[]>(this.getCategoriesUrl).pipe(
      catchError(this.handleError)
    );
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error("An error occurred:", error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError(error);
  }
}
